package br.com.badbear.inspetor;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Alexandre on 16/04/2015.
 */
public class GetPhotoTask extends AsyncTask<String,Void,Bitmap>
{
     public interface Callback
     {
         public void photoReceived(Bitmap photo);
     }

     private Callback callback;

     public GetPhotoTask(Callback callback)
     {
         this.callback = callback;
     }

    @Override
    protected Bitmap doInBackground(String... params) {
//            String host = "10.0.3.2";
            String host = "catraca.azurewebsites.net";
            String port = "";

            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL("http://"+host+port+"/api/Breach/Photo/"+params[0]).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        callback.photoReceived(bitmap);
    }
}
