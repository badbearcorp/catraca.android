package br.com.badbear.inspetor;

import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Alexandre on 16/04/2015.
 */
public class GetPhotosIdsTask extends AsyncTask<Void,Void,String[]>
{
     public interface Callback
     {
         public void idsReceived(String[] ids);
     }

     private Callback callback;

     public GetPhotosIdsTask(Callback callback)
     {
         this.callback = callback;
     }

    @Override
    protected String[] doInBackground(Void... params) {
        HttpClient client = AndroidHttpClient.newInstance("Android");
        try {

//            String host = "10.0.3.2";
            String host = "catraca.azurewebsites.net";
            String port = "";

            HttpGet post = new HttpGet("http://"+host+port+"/api/Breach/PhotoIds");
            post.addHeader("Accept","application/json");
            post.addHeader("Content-Type","application/json");

            HttpResponse response = client.execute(post);
            InputStream in = response.getEntity().getContent();

            InputStreamReader reader = new InputStreamReader(in);
            BufferedReader b = new BufferedReader(reader);
            String resultado = "";
            String line;
            while((line = b.readLine())!= null)
                resultado+=line+"\n";

            if(response.getStatusLine().getStatusCode()!=200)
            {
                return null;
            }
            else
            {
                JSONArray arr = new JSONArray(resultado);
                String[] retVal = new String[arr.length()];

                for(int x=0;x<arr.length();x++)
                {
                    retVal[x] = arr.getString(x);
                }
                return retVal;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        finally {
            ((AndroidHttpClient)client).close();
        }
    }

    @Override
    protected void onPostExecute(String[] strings) {
        super.onPostExecute(strings);
        if(strings == null)
        {
            Log.e("Inspetor", "strings null");
        }
        else {
            callback.idsReceived(strings);
        }
    }
}
