package br.com.badbear.inspetor;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by Alexandre on 16/04/2015.
 */
public class ImageAdapter extends BaseAdapter implements GetPhotosIdsTask.Callback,GetPhotoTask.Callback{

    private ArrayList<Bitmap> bitmaps = new ArrayList<>();
    private Context context;
    private LayoutInflater l_Inflater;

    private static ImageAdapter instance;

    public static ImageAdapter getInstance(Context ctx)
    {
        if(instance == null)
            instance = new ImageAdapter(ctx);
        else if(instance.context == null || !instance.context.equals(ctx))
            instance.context = ctx;

        return instance;
    }

    private ImageAdapter(Context context)
    {
        l_Inflater = LayoutInflater.from(context);
        (new GetPhotosIdsTask(this)).execute();
    }


    @Override
    public int getCount() {
        return bitmaps.size();
    }

    @Override
    public Object getItem(int position) {
        return bitmaps.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null)
            convertView = l_Inflater.inflate(R.layout.grid_item, null);

        ImageView iv = (ImageView)convertView.findViewById(R.id.imageView);
        iv.setImageBitmap(bitmaps.get(position));

        return convertView;
    }

    @Override
    public void idsReceived(String[] ids) {
        for(String id : ids)
        {
            GetPhotoTask task = new GetPhotoTask(this);
            task.execute(id);
        }
    }

    @Override
    public void photoReceived(Bitmap photo) {
        bitmaps.add(0,photo);
        notifyDataSetChanged();
    }
}
