package br.com.badbear.inspetor;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!checkPlayServices())
        {
            Toast.makeText(getApplicationContext(), "Google Play nao disponivel.", Toast.LENGTH_LONG);
            return;
        }

        PreferencesUtil.init(getApplicationContext());

        PreferencesUtil.STATE state = PreferencesUtil.getState();
        if(state == PreferencesUtil.STATE.REGISTER) {
            RegistrationTask task = new RegistrationTask(this);
            task.execute();
        }

        setContentView(R.layout.activity_main);

        GridView gv = (GridView)findViewById(R.id.gridView);
        gv.setAdapter(ImageAdapter.getInstance(getApplicationContext()));
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        9000).show();
            } else {
                Log.i("colegio", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

}
