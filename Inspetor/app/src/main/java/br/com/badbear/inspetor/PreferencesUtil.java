package br.com.badbear.inspetor;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesUtil {

    public enum STATE {REGISTER, DONE}

    private static final String SHARED_NAME = "InspetorPreferences";
    private static Context appContext = null;

    public static void init(Context appContext)
    {
        PreferencesUtil.appContext = appContext;
    }

    private static SharedPreferences getPreferences()
    {
        if(appContext == null)
            throw new IllegalStateException("PreferencesUtil nao foi inicializado. Execute PreferencesUtil.init na criação da aplicação");
        return appContext.getSharedPreferences(SHARED_NAME,Context.MODE_PRIVATE);

    }

    public static String getRegistrationId()
    {
        return getPreferences().getString("regid",null);
    }

    public static void setRegistrationId(String id)
    {
        getPreferences().edit().putString("regid",id).commit();
    }

    public static STATE getState()
    {
        STATE retVal = STATE.REGISTER;
        String state = getPreferences().getString("state",null);
        if(state != null)
            retVal = STATE.valueOf(state);
        return retVal;
    }

    public static void setState(STATE state)
    {
        getPreferences().edit().putString("state",state.toString()).commit();
    }
}
