package br.com.badbear.inspetor;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;



/**
 * Created by Alexandre on 27/12/2014.
 */
public class RegistrationTask extends AsyncTask<Void, Void, Boolean> {

    private Activity parent;
    private ProgressDialog pd;

    public RegistrationTask(Activity parent)
    {
        this.parent = parent;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        parent.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pd = new ProgressDialog(parent);
                pd.setTitle("Registrando Aplicativo");
                pd.setMessage("Por favor aguarde.");
                pd.setCancelable(false);
                pd.setIndeterminate(true);
                pd.show();

            }
        });
    }


    @Override
    protected Boolean doInBackground(Void... params) {

        String gcm = registerGCM();
        if(gcm == null)
            return false;

        HttpClient client = AndroidHttpClient.newInstance("Android");
        try {

//            String host = "10.0.3.2";
            String host = "catraca.azurewebsites.net";
            String port = "";
            HttpPost post = new HttpPost("http://"+host+port+"/api/Register");
            post.addHeader("Accept","application/json");
            post.addHeader("Content-Type","application/json");

            JSONObject obj = new JSONObject();
            obj.put("gcm", gcm);
            obj.put("device", Build.DEVICE);

            post.setEntity(new StringEntity(obj.toString()));

            HttpResponse response = client.execute(post);
            InputStream in = response.getEntity().getContent();

            InputStreamReader reader = new InputStreamReader(in);
            BufferedReader b = new BufferedReader(reader);
            String resultado = "";
            String line;
            while((line = b.readLine())!= null)
                resultado+=line+"\n";

            if(response.getStatusLine().getStatusCode()!=200)
            {
                return false;
            }

            PreferencesUtil.setState(PreferencesUtil.STATE.DONE);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        finally
        {
            ((AndroidHttpClient)client).close();
        }
    }

    private String registerGCM() {
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(parent.getApplicationContext());
        try {
            String regid = gcm.register("400098356038");
            return regid;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    protected void onPostExecute(Boolean result)
    {
        pd.cancel();
        if(!result)
        {
            parent.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(parent.getApplicationContext(),"Não foi possível registrar o app.\nFavor tentar mais tarde.",Toast.LENGTH_LONG).show();
                        new Thread(){
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(3500); // LENGTH_SHORT = 2000
                                    parent.finish();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }.start();
                 }
                });
        }
    }
}
